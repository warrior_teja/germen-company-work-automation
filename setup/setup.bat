REM ECHO OFF
REM To make these comments or any other print statements visible remove echo off

set batspath=%CD%
REM CD %batspath %\Modules
REM echo %batspath %
REM CALL "ModulesToBeInstalled.bat"

CD ..
mkdir R1.0 && CD R1.0
CALL "%batspath%\impfiles\createvenv.bat"
CALL "%batspath%\impfiles\activatevenv.bat"
REM CALL "%batspath%\impfiles\ModulesToBeInstalled.bat"
pause
echo %CD%
CALL "%batspath%\impfiles\deactivate.bat"

REM next step is to delete
pause
CD %batspath%
CD ..
DEL R1.0
pause
